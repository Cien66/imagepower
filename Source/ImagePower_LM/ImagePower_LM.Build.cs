// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ImagePower_LM : ModuleRules
{
	public ImagePower_LM(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
