
#pragma once

#include "UObject/Interface.h"
#include "InteractionInterface.generated.h"

UINTERFACE(MinimalAPI)
class UInteractionInterface : public UInterface
{
	GENERATED_BODY()
};

class IInteractionInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Interaction)
		void ShowInteractionSymbol();
		virtual void ShowInteractionSymbol_Implementation() {}
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Interaction)
		void HideInteractionSymbol();
		virtual void HideInteractionSymbol_Implementation() {}

	UFUNCTION(BlueprintImplementableEvent, Category = Interaction)
		void Interaction();
		//virtual void Interaction_Implementation() {}

	bool GetIsShow() const { return IsShow; }

protected:
	bool IsShow{false};
};
