// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ImagePower_LMGameMode.generated.h"

UCLASS(minimalapi)
class AImagePower_LMGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AImagePower_LMGameMode();
};



