
#pragma once

#include "GameFramework/Character.h"
#include "ImagePower_LMCharacter.generated.h"

class UInputComponent;

UCLASS(config=Game)
class AImagePower_LMCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	AImagePower_LMCharacter();

protected:	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
		float BaseTurnRate{180.f};

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
		float BaseLookUpRate{45.f};

	FORCEINLINE void SetCanMove(bool InCanMove) { CanMove = InCanMove; }
	FORCEINLINE bool GetCanMove() const { return CanMove; }

protected:
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void Tick(float DeltaSeconds) override;

	void MoveForward(float Val);
	void MoveRight(float Val);

	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

	void Interaction();
	void TrySpawnMinion();

	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	UPROPERTY(EditAnywhere, Category = Minion)
		TSubclassOf<class AMinionActor> MinionClass;

	UPROPERTY(EditAnywhere, Category = Minion)
		int MaxMinions{5};

private:
	class AMinionActor* SpawnMinion();

	TArray<FHitResult> GetCameraHits() const;

	void OnStartMitionInteraction(const TWeakObjectPtr<AActor>& InActor);
	void OnEndMitionInteraction(const TWeakObjectPtr<AActor>& InActor);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FirstPersonCameraComponent{nullptr};

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float LenghtTrace{200.f};

	bool CanMove{true};

	TArray<TWeakObjectPtr<class AMinionActor>> Minions;
};

