// Copyright Epic Games, Inc. All Rights Reserved.

#include "ImagePower_LMCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"

#include "AI/MinionActor.h"
#include "Interfaces/InteractionInterface.h"

#include <Runtime/Engine/Classes/Kismet/KismetMathLibrary.h>

AImagePower_LMCharacter::AImagePower_LMCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
}

void AImagePower_LMCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Interaction", IE_Pressed, this, &AImagePower_LMCharacter::Interaction);
	PlayerInputComponent->BindAction("SpawnMinion", IE_Pressed, this, &AImagePower_LMCharacter::TrySpawnMinion);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AImagePower_LMCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AImagePower_LMCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AImagePower_LMCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AImagePower_LMCharacter::LookUpAtRate);
}

void AImagePower_LMCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	for(const TWeakObjectPtr<AMinionActor>& Minion : Minions)
	{
		Minion->OnStartInteractionDelegate.Unbind();
		Minion->OnEndInteractionDelegate.Unbind();
	}
}

void AImagePower_LMCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	const TArray<FHitResult> Hits = GetCameraHits();

	for(const FHitResult& Hit : Hits)
		if(Hit.Actor->Implements<UInteractionInterface>())
			IInteractionInterface::Execute_ShowInteractionSymbol(Hit.GetActor());
}

void AImagePower_LMCharacter::MoveForward(float Value)
{
	if(GetCanMove() && Value != 0.0f)
		AddMovementInput(GetActorForwardVector(), Value);
}

void AImagePower_LMCharacter::MoveRight(float Value)
{
	if(GetCanMove() && Value != 0.0f)
		AddMovementInput(GetActorRightVector(), Value);
}

void AImagePower_LMCharacter::TurnAtRate(float Rate)
{
	if(GetCanMove())
		AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AImagePower_LMCharacter::LookUpAtRate(float Rate)
{
	if(GetCanMove())
		AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AImagePower_LMCharacter::Interaction()
{
	const TArray<FHitResult> Hits = GetCameraHits();

	for(const FHitResult& Hit : Hits)
		if(IInteractionInterface* Interaction = Cast<IInteractionInterface>(Hit.GetActor()))
			if(Interaction->GetIsShow())
				IInteractionInterface::Execute_Interaction(Hit.GetActor());
}

void AImagePower_LMCharacter::TrySpawnMinion()
{
	if(Minions.Num() >= MaxMinions)
		return;

	TWeakObjectPtr<AMinionActor> Minion = SpawnMinion();

	if(Minion.IsValid())
	{
		Minion->OnStartInteractionDelegate.BindUObject(this, &AImagePower_LMCharacter::OnStartMitionInteraction);
		Minion->OnEndInteractionDelegate.BindUObject(this, &AImagePower_LMCharacter::OnEndMitionInteraction);
		Minions.Add(Minion);
	}
}

AMinionActor* AImagePower_LMCharacter::SpawnMinion()
{
	if(UWorld* World = GetWorld())
	{
		const FVector SpawnLocation = GetActorLocation() + GetActorForwardVector() * 300.f;
		const FRotator SpawnRotator = GetActorRotation();

		FActorSpawnParameters ASP;
		ASP.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

		return World->SpawnActor<AMinionActor>(MinionClass, SpawnLocation, SpawnRotator, ASP);
	}

	return nullptr;
}

TArray<FHitResult> AImagePower_LMCharacter::GetCameraHits() const
{
	TArray<FHitResult> Hits;
	if(const UWorld* World = GetWorld())
	{
		const FVector& Start = FirstPersonCameraComponent->GetComponentLocation();
		const FVector& End = Start + FirstPersonCameraComponent->GetForwardVector() * LenghtTrace;

		FCollisionQueryParams CQP;
		CQP.AddIgnoredActor(this);

		World->LineTraceMultiByChannel(Hits, Start, End, ECollisionChannel::ECC_Camera, CQP);
	}

	return Hits;
}

void AImagePower_LMCharacter::OnStartMitionInteraction(const TWeakObjectPtr<AActor>& InActor)
{
	SetCanMove(false);

	/*if(InActor.IsValid())
	{
		const FRotator& Rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), InActor->GetActorLocation());
		GetFirstPersonCameraComponent()->SetWorldRotation(Rotation);
	}*/

	if(APlayerController* PlayerController = GetController<APlayerController>())
	{
		PlayerController->SetInputMode(FInputModeUIOnly());

		PlayerController->bShowMouseCursor = true;
		PlayerController->bEnableMouseOverEvents = true;
		PlayerController->bEnableClickEvents = true;
	}
}

void AImagePower_LMCharacter::OnEndMitionInteraction(const TWeakObjectPtr<AActor>& InActor)
{
	SetCanMove(true);

	if(APlayerController* PlayerController = GetController<APlayerController>())
	{
		PlayerController->SetInputMode(FInputModeGameOnly());

		PlayerController->bShowMouseCursor = false;
		PlayerController->bEnableMouseOverEvents = false;
		PlayerController->bEnableClickEvents = false;
	}
}
