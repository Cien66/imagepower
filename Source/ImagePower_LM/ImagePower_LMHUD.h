
#pragma once 

#include "GameFramework/HUD.h"
#include "ImagePower_LMHUD.generated.h"

UCLASS()
class AImagePower_LMHUD : public AHUD
{
	GENERATED_BODY()

public:
	AImagePower_LMHUD();

	virtual void DrawHUD() override;

private:
	class UTexture2D* CrosshairTex;

};

