// Copyright Epic Games, Inc. All Rights Reserved.

#include "ImagePower_LMGameMode.h"
#include "ImagePower_LMHUD.h"
#include "ImagePower_LMCharacter.h"
#include "UObject/ConstructorHelpers.h"

AImagePower_LMGameMode::AImagePower_LMGameMode()
	: Super()
{
	//// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	//DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AImagePower_LMHUD::StaticClass();
}
