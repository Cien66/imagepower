
#pragma once

#include "GameFramework/Character.h"
#include "../Interfaces/InteractionInterface.h"
#include "MinionActor.generated.h"

DECLARE_DELEGATE_OneParam(FOnInteractionDelegate, const TWeakObjectPtr<AActor>&)

UCLASS()
class AMinionActor: public ACharacter, public IInteractionInterface
{
	GENERATED_BODY()

public:
	AMinionActor();

public:
	FOnInteractionDelegate OnStartInteractionDelegate;
	FOnInteractionDelegate OnEndInteractionDelegate;

	UFUNCTION(BlueprintNativeEvent, Category = Interaction)
		void ShowInteractionSymbol();
		virtual void ShowInteractionSymbol_Implementation() override;
	UFUNCTION(BlueprintNativeEvent, Category = Interaction)
		void HideInteractionSymbol();
		virtual void HideInteractionSymbol_Implementation() override;

	UFUNCTION(BlueprintImplementableEvent, Category = Interaction)
		void Interaction();
		//virtual void Interaction_Implementation() override;

protected:
	UFUNCTION(BlueprintCallable, Category = Minion)
		void OnInteraction();
	
	UFUNCTION(BlueprintCallable, Category = Minion)
		void OnHaveTask();


	UPROPERTY(EditAnywhere, Category = Interaction)
		float DelayHideInteractionSymbol{0.2f};

private:
	FTimerHandle TimerHandle;
};
