
#include "MinionActor.h"
#include <Runtime/Engine/Public/TimerManager.h>

AMinionActor::AMinionActor()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AMinionActor::ShowInteractionSymbol_Implementation()
{
	IsShow = true;

	GetWorldTimerManager().ClearTimer(TimerHandle);
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AMinionActor::HideInteractionSymbol, DelayHideInteractionSymbol);
}

void AMinionActor::HideInteractionSymbol_Implementation()
{
	IsShow = false;
}

void AMinionActor::OnInteraction()
{
	OnStartInteractionDelegate.ExecuteIfBound(this);
}

void AMinionActor::OnHaveTask()
{
	OnEndInteractionDelegate.ExecuteIfBound(this);
}

//Blueprint parent dont call -.-'

//void AMinionActor::Interaction_Implementation()
//{
//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "INTERACTION");
//	OnInteractionDelegate.ExecuteIfBound();
//}
